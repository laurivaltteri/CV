# 👨‍🔬 Curriulum Vitae

    1.12.2020
# Lauri Ahonen

**PhD** (cognitive science) || **MSc** (technology) <br/>
lauri@protonmail.com || +358 43 824 0302

----------
## 📱 Online Profiles

[LinkedIn](https://linkedin.com/in/laurivaltteri/)<br/>
[ResearchGate](https://researchgate.net/profile/Lauri_Ahonen)<br/>
[GitHub](https://github.com/laurivaltteri/)

----------
## ♈️ Biography

My strengths are neuroscience twist in my analytics expertise and experience in talented teams in data science. I’m a digital professional with profound understanding on novel data analytics and data tools. Furthermore, the engineering-psychology foundation ensures understanding potential and caveats of data analytics and statistical biases. 

My background in academia has provided a wide outlook over digital health data with over ten years of experience in personalized health. The experience in science has also yielded excellent capabilities in international collaboration, strong proficiency in independent work, and organising and managing projects. Working in multidisciplinary teams has flourished skills in resolving conflicts and resulted a solution-oriented mindset. Despite the independent attitude, research projects have also thrived behaviour to actively ask for guidance.

I've been involved in multiple applied research and basic research projects with physiological measurements, please see the *Skills* section for modalities. The studies topics have involved sleep research, circadian rhythms, psychophysics, models of pgysiological systems, executive functions, situational awareness and multimodal perception, burn-out, and autonomic nervous system responses to emotional-cognitive factors such stress.

My experience in cutting edge R&D teams has provided an extensive practical competence of current data tools, methods, and technologies. Moreover, my endeavour in scale-up business environment has provided an excellent position to gain experience in agile methods and to participate in product development and implementation of modern architectures. Working in a small company, constantly redefining goals has enhanced perseverant but yet resilient mindset. 

As an analytic personality, I believe the job of a data scientist is to always think of the big picture. It is important to be able to identify what is relevant and to focus on that. And I can't emphasize enough the relevance of utilizing state of art tools in data architectures.


----------
## 🎯 Skills

R, Python, UNIX, MATLAB, EEG, MEG, ECG, PPG, Actigraphy, EDA, exprience sampling, Tidymodels, Tensorflow, Tableau, SQL, git, Docker, Kubernetes, AWS, Data Fusion, Feature Extraction, Bayesian Analysis, Classification Techniques, Graph-theory, Signal Processing, Computational Neuroscience, Health Technology, IoT, Academic Communications, Presenting, Teaching, Project Management, AI, Cognitive Psychology

----------
## 🎓 Education

- Doctor of Philosophy
  - University of Helsinki
  - Major: **Cognitive science**
  - Thesis: [Quantifying Cognition: Applications for Ubiquitous Data](https://helda.helsinki.fi/handle/10138/235507)
- Master of Science in Technology
  - Aalto University
  - Major: **Cognitive technology**
  - Thesis: [a Computational Approach to Estimation of Crowding in Natural Images](http://viXra.org/abs/1802.0066)

----------
## 📚 Other Studies and Training

- Visiting Researcher
  - McGill University (Montreal, Quebec, Canada) 2013
    - Department of Biomedical Engineering and the School of Computer Science
  - Université de Montréal (Québec, Canada) 2012
    - BRAMS – International Laboratory for Brain, Music and Sound Research
- Other Certificates
  - Product Development (Specialist Qualification) 2012
    -  Finnish National Agency for Education (EDUFI)

----------
## 💼 Work history
- Senior Data Scientist (Nightingale Health Ltd.) 2018 <br/>
  - Main projects: development of development environment and processes (DevOps), platform and process development for biomarker extraction (DS), development of risk models and predictions from biobank population data (DS)
  - My main role was leading and supporting scientific analyses for internal R&D and external dissemination. I was involved in development of data science environment and processes with DevOps team. However, my main contributions are in process and analysis development of the biomarker extraction product and product development for predicting risk scores in population models.
- Research Engineer (Finnish Institute of Occupational Health) 2009
  - Projects: **Seamless patient care (Tekes), Burnout in the brain at work (Academy of Finland), Revolution of Knowledge Work (Tekes)** <br/>
  I prepared my PhD. I took part in research funding applications, and presented results in international forums. I was responsible in setting up research paradigms and analysis of results. I gave lectures and planned the projects with collaborators and customers. I was also leading projects. I participated and conducted sleep studies, experiments utilizing EEG, and MEG studies, as well as, paradigms with autonomous nervous systems recordings and experience sampling. As part of my own research I was responsible for processing and analyzing the research data. In addition, I took part to other studies as a data analyst and data engineer. I have profound experience in time-series analysing and for various source separation toolkits.
- Laboratory Engineer (Finnish Institute of Occupational Health) 2008
  - Projects: **SalWe -- enabling research for health and well-being (Tekes/EU), Supporting situation awareness in demanding operating conditions through wearable multimodal user interfaces (Tekes)**  <br/>
  Supporting research paradigm development, conducting measurements, supporting laboratory maintenance and development, preparing literature reviews. 
- Research Assistant (Finnish Institute of Occupational Health) 2007
  - Projects: **Brain & Work: multi-tasking at work (Tekes)** <br/>
  I was hired to conduct a literature reviews and ended up preparing my master’s thesis. I was also supporting the measurements, and maintaining laboratories.

----------
## 📄 Journal Publications
- Sari Ylinen, Katja Junttila, Marja Laasonen, Paul Iverson, Lauri Ahonen, Teija Kujala *Diminished brain responses to second-language words are linked with native-language literacy skills in dyslexia*, Neuropsychologia S0028-3932(18)30746-2, 11/2018. [DOI: 10.1016/j.neuropsychologia.2018.11.005](https://doi.org/10.1016/j.neuropsychologia.2018.11.005)
- Lauri Ahonen, Benjamin Cowley, Arto Hellas, Kai Puolamäki *Biosignals reflect pair-dynamics in collaborative work: EDA and ECG study of pair-programming in a classroom environment.* Scientific Reports 8(1):3138, 02/2018. [DOI:10.1038/s41598-018-21518-3](https://doi.org/10.1038/s41598-018-21518-3)
- Benjamin Cowley, Marco Filetti, Kristian Lukander, Jari Torniainen, Andreas Helenius, Lauri Ahonen, Oswald Barral Mery de Bellegarde, Johannes Kosunen, Teppo Valtonen, Minna Huotilainen, Jaakko Niklas Ravaja, Giulio Jacucci *The Psychophysiology Primer: A Guide to Methods and a Broad Review with a Focus on Human–Computer Interaction.* Foundations and Trends® in Human-Computer Interaction 9(3-4):151-308, 11/2016. [DOI: 10.1561/1100000065](https://doi.org/10.1561/1100000065)
- Lauri Ahonen, Benjamin Cowley, Jari Torniainen, Antti Ukkonen, Arto Vihavainen, Kai Puolamäki *Cognitive Collaboration Found in Cardiac Physiology: Study in Classroom Environment*. PLoS ONE 11(7):e0159178, 07/2016. [DOI:10.1371/journal.pone.0159178](https://doi.org/10.1371/journal.pone.0159178)
- Jussi Korpela, Andreas Henelius, Lauri Ahonen, Arto Klami, Kai Puolamäki *Using regression makes extraction of shared variation in multiple datasets easy*. Data Mining and Knowledge Discovery 30(5):1112–1133, 05/2016. [DOI:10.1007/s10618-016-0465-y](https://doi.org/10.1007/s10618-016-0465-y)
- Laura Sokka, Marianne Leinikka, Jussi Korpela, Andreas Henelius, Lauri Ahonen, Claude Alain, Kimmo Alho, Minna Huotilainen *Job burnout is associated with dysfunctions in brain mechanisms of voluntary and involuntary attention*. Biological psychology 117:56-66, 03/2016. [DOI:10.1016/j.biopsycho.2016.02.010](https://doi.org/10.1016/j.biopsycho.2016.02.010)
- Lauri Ahonen, Minna Huotilainen, Elvira Brattico *Within- and between-session replicability of cognitive brain processes: An MEG study with an N-back task*. Physiology & Behavior 158:43–53, 02/2016. [DOI:10.1016/j.physbeh.2016.02.006](https://doi.org/10.1016/j.physbeh.2016.02.006)

----------
## 📄 Conference Proceedings

- Kati Pettersson, Lauri Ahonen, Laura Sokka, Kiti Müller, Satu Pakarinen *Simulated knowledge work decreases alertness in job-burnout.* SLEEP Abstract Supplements, 06/2018
- Miika Leminen, Lauri Ahonen, Matti Gröhn, Minna Huotilainen, Tiina Paunio, Jussi Virkkala *Comparing auditory stimuli for sleep enchancement: mimicking a spleeping situation*. International Conference on Auditory Display Abstract Supplements, 01/2014
- Lauri Ahonen, Minna Huotilainen *Frontal asymmetry over EEG power spectra*. OHBM Annual Meeting Supplements, 01/2014
- Minna Huotilainen, Lauri Ahonen *Decrease of processing speed due to switching between tasks*. Society for Psychophysiological Research Annual Meeting Proceedings, 01/2013
- Lauri Ahonen, Minna Huotilainen *Increasing functional connectivity with cognitive load*. Society for Neuroscience Annual Meeting Program, 01/2013
- Lauri Ahonen, Matti Gröhn, Minna Huotilainen, Sharman Jagadeesan, Tiina Paunio, Jussi Virkkala *Designing auditory stimulus for sleep enhancement*. Conference papers and extended abstracts, 01/2013
- Lauri Ahonen, Minna Huotilainen *Evaluating the effect of mood on cognitive performance*. World Congress on Medical Physics and Biomedical Engineering Proceedings, 01/2012
- Matti Gröhn, Lauri Ahonen, Minna Huotilainen *Some effects of continuous tempo and pitch transformations in perceived pleasantness of listening to a musical sound file*. International Conference on Auditory Display Abstract Supplements, 01/2011
- Lauri Ahonen, Minna Huotilainen *Mood induction by music and its effects in cognitive tests*. Poster Abstract Supplements, 01/2010
- Risto Näsänen, Lauri Ahonen, Sharman Jagadeesan, Kiti Müller *Cueing spatial visual attention by symbolic and directional auditory stimuli*. EVPC Abstract Supplements, 01/2008

----------
## 📄 Technical Reports

- Patrik Floréen, Salvatore Andolina, Aristides Gionis, Andrej Gisbrecht, Dorota Głowacka, Giulio Jacucci, Markus Koskela, Kai Kuikkaniemi, Tuukka Lehtiniemi, Matti Nelimarkka, Jaakko Peltonen, Tuukka Ruotsalo, Miamaria Saastamoinen, Baris Serim, Mats Sjöberg, Jonathan Strahl, Tung Vuong, Benjamin Cowley, Kai Puolamäki, Marco Filetti, Mohammad Hoque, Andreas Henelius, Kristian Lukander, Antti Ukkonen *Revolution of Knowledge Work A Tekes Large Strategic Opening Final Reports.* 2015 & 2017
- Paula Valkonen, Lauri Ahonen, Juhani Heinilä, Immo Heino, Jukka Häkkinen, Simo Järvelä, Inger Ekman, Kari Kallinen, Jari Laarni, Kristian Lukander, Timo Urhemaa, Teppo Valtonen, Antti Väätänen *AJAN TASALLA — Tilannetietoisuutta tukevat käyttöliittymät vaativissa toimintaympäristöissä.* 2010.

----------
